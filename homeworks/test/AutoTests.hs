module AutoTests where

-- Do not change! It will be overwritten!
-- Use MyTests.hs to define your tests

import Test.Tasty (TestTree(..), testGroup, Timeout(..), localOption)
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck

import Lib

exampleTests :: [TestTree]
exampleTests =
  [ testGroup "HW 0"
    [ testGroup "HW 0.0"
      [ testCase "Works for Alice" $ hw0_0 "Alice" @?= "Hello, Alice"
      , testCase "Works for Bob"   $ hw0_0 "Bob"   @?= "Hello, Bob"
      ]
    ]
  , localOption (Timeout 1000000 "1s") $ testGroup "HW 1"
    [ testGroup "HW 1.1"
      [ testCase "Works for 2 and 3"    $ hw1_1 2 3    @?= 5
      , testCase "Works for 3 and 1000" $ hw1_1 3 1000 @?= 1003
      ]
    , testGroup "HW 1.2"
      [ testCase "Works on 2" $ hw1_2 2 @?= 1.25 ]
    , testGroup "n!!"
      [ testCase "3!!" $ fact2 3 @?= 3
      , testCase "4!!" $ fact2 4 @?= 8
      ]
    , testGroup "isPrime"
      [ testCase "isPrime 13" $ isPrime 13 @?= True
      , testCase "isPrime 12" $ isPrime 12 @?= False
      , testCase "isPrime 2"  $ isPrime 2  @?= True
      , testCase "isPrime 1"  $ isPrime 1  @?= False
      ]
    , testGroup "primeSum"
      [ testCase "primeSum 2 3" $ primeSum 2 3 @?= 5
      , testCase "primeSum 3 2" $ primeSum 3 2 @?= 0
      , testCase "primeSum 2 2" $ primeSum 2 2 @?= 2
      , testCase "primeSum 1 10" $ primeSum 1 10 @?= 17
      ]
    ]
  , localOption (Timeout 1000000 "1s") $ testGroup "HW 2"
    [ testGroup "isKnown"
      [ testCase "On"  $ isKnown On  @?= True
      , testCase "Off" $ isKnown Off @?= True
      , testCase "Unknown" $ isKnown Unknown @?= False
      ]
    , testGroup "eval"
      [ testCase "eval 2+3*(4+5)*(6-5)" $ eval (Add
                                                (Const 2)
                                                (Mult
                                                 (Const 3)
                                                 (Mult
                                                  (Add (Const 4) (Const 5))
                                                  (Sub (Const 6) (Const 5))))) @?= 29
      ]
    , testGroup "simplify"
      [ testCase "simplify 2+3*(4+5)*(6-5)" $ eval (simplify (Add (Const 2) (Mult (Const 3) (Mult (Add (Const 4) (Const 5)) (Sub (Const 6) (Const 5)))))) @?= 29
      ]
    ]
  ]

oldTests :: [TestTree]
oldTests = [
  testGroup "HW 0"
    [ testGroup "HW 0.0"
      [ testCase "Works for Eve" $ hw0_0 "Eve" @?= "Hello, Eve"
      , testProperty "Works for any string" $ \s -> hw0_0 s === ("Hello, " ++ s)
      ]
    ]

  , localOption (Timeout 100000 "0.1s") $ testGroup "HW 1"
    [ testGroup "HW 1.1"
      [ testCase "Works for -1000 and 1000" $ hw1_1 (-1000) 1000 @?= 0
      , testProperty "Works for any a and b" $ \a b -> hw1_1 a b === a + b
      ]
    , testGroup "HW 1.2"
      [ testProperty "Works for any n" $ \n -> (n>0) ==> hw1_2 n `approx` sum (reverse [1.0 / fromIntegral (k^k) | k<-[1..n]])
      ]
    , testGroup "n!!"
      [ localOption (Timeout 1000000 "1s") $ testCase "0!!" $ fact2 0 @?= 1
      , testCase "1!!" $ fact2 1 @?= 1
      , localOption (Timeout 1000000 "1s") $ testProperty "Works for any n" $ \n -> (n>=0) ==> fact2 n === product (filter (\k -> odd k == odd n) [1..n])
      ]
    , testGroup "isPrime"
      [ testCase "isPrime 49"  $ isPrime 49 @?= False
      , testCase "isPrime 257" $ isPrime 257 @?= True
      , testCase "isPrime 32771*32769" $ isPrime (32771*32769) @?= False
      , testProperty "Works for any number" $ \n -> (n>=1) ==> isPrime n === myIsPrime n
      , testProperty "Works for any product" $ \a b -> (a>1 && b>1) ==> isPrime (a*b) === False
      ]
    , testGroup "isPrime is not slow"
      [ localOption (Timeout 10000000 "10s") $ testCase "isPrime 10^10 + 19" $ isPrime 10000000019 @?= True
      ]
    , testGroup "primeSum"
      [ testProperty "Works for any interval" $ \a b -> (a>0 && b>0) ==> primeSum a b === sum (filter myIsPrime [a..b])
      ]
    ]
  ]
  where approx :: Double -> Double -> Bool
        approx x1 x2 = abs (x1 - x2) < 0.000001
        
        myIsPrime :: Integer -> Bool
        myIsPrime 1 = False
        myIsPrime n = all (\p->n`mod`p/=0) $ takeWhile (\i->i*i<=n) $ 2:[3,5..n]

autoTests :: [TestTree]
autoTests = [ ]

